let d = document;
let unsafeWindow = window.wrappedJSObject;

// Создаёт список скриптов в хедере
function insertScriptList()
{
    let listCreated = d.querySelector('.mw-scripts-list-block');

    if (!listCreated)
    {
        let insBeforeSearch = d.querySelector('.search-block');
        let hiAllButton = d.createElement('div');

        hiAllButton.className = 'user-wrap pull-right ng-scope mw-scripts-list-block';
        hiAllButton.id = "scripts-list-block";
        hiAllButton.style.height = '48px';

        hiAllButton.innerHTML = 'Скрипты' +
            '<ul id="scripts-list" class="mw-scripts-list">' +
            '<li class="mw-script-li mw-execute-scripts">Выполнить скрипты</li>' +
            '<li class="mw-script-li mw-take-clip-url" title="Работает только на те клипы, что уже раскрыты на странице">Получить ссылки на клипы</li>' +
            '</ul>';

        insBeforeSearch.parentElement.insertBefore(hiAllButton, insBeforeSearch);
    }
}

// Иноформация о стриме под галкой
function insertControversialStreamInfo()
{
    let controversialInfo = d.createElement('div');
    let insBeforeDashElem = d.querySelector('.streamer-dashboard > .dash-element:first-of-type');
    let streamInfo = unsafeWindow.channelTopInfo;
    let existedBlock = d.querySelector('.mw-controversial-info');

    if (!existedBlock && insBeforeDashElem && streamInfo)
    {
        let hidden = streamInfo.channel.hidden;

        controversialInfo.className = 'dash-element mw-controversial-info';
        controversialInfo.innerHTML = '<div style="color: #dfecff;"><span>Спорный контент</span></div>';

        if (hidden == 1)
        {
            insBeforeDashElem.parentElement.insertBefore(controversialInfo, insBeforeDashElem);
        }
    }
}

// Получить ссылки на раскрытые клипы
function takeLinkToClip()
{
    let clipPanels = d.querySelectorAll('gg-player-panel .control-inner.clearfix');
    let progressBars = d.querySelectorAll('gg-player-panel .progress-bar');

    if (clipPanels && progressBars)
    {
        if (clipPanels.length !== progressBars.length)
        {
            console.error('Количество прогресс баров не соответствует клип-панелям. Если вы видите это, то, вероятно, произошло что-то ужасное.');
            return false;
        }

        for (let i = 0; i < clipPanels.length; i++)
        {
            let linkId = clipPanels[i].closest('gg-embed-video').id;
            let link = 'https://goodgame.ru/clip/' + linkId + '/';
            let insBefore = clipPanels[i].querySelector('.pull-right > a:first-of-type');

            let goToSource = document.createElement('a');
            goToSource.className = "player-button pull-left";
            goToSource.setAttribute('target', '_blank');
            goToSource.innerHTML = '<div class="state"><div class="tip">Перейти на страницу клипа</div><span class="icon icon-mention-streamer" ' +
                                   'style="padding: 11px 15px;"></span></div>';
            goToSource.setAttribute('href', link);

            insBefore.parentElement.insertBefore(goToSource, insBefore);
        }
    }
    else
    {
        console.info('Клипы не найдены')
    }
}

// Вставка кнопки сворачивания и разворачивания сообщения
function messageMinimizeBtn(message)
{
    let messageHeight = parseInt(getComputedStyle(message).height);
    if (messageHeight > 36)
    {
        let userName = message.querySelector('chat-user');
        let minimizeBtn = document.createElement('span');

        minimizeBtn.innerHTML = '‒';
        minimizeBtn.className = 'mw-minimize-maximize-btn icon';
        minimizeBtn.setAttribute('title', 'Свернуть сообщение');

        userName.parentElement.insertBefore(minimizeBtn, userName);
    }
}

// Сворачивание списка смайлов
function minimizeStreamerSmiles()
{
    let minimizeSymbol = '‒';
    let plusSymbol = '&#10133;';
}

// функция обсервера чата для его модифицирования
function startObserving()
{
    let chat = document.querySelector('ng-transclude > .chat-section.ng-scope');

    let observer = new MutationObserver(function (mutations)
    {
        mutations.forEach(function (mutation)
        {
            if (mutation.addedNodes.length)
            {
                for (let i = 0; i < mutation.addedNodes.length; i++)
                {
                    if (mutation.addedNodes[i].tagName == 'DIV')
                    {
                        // console.log(mutation.addedNodes[i]);
                        messageMinimizeBtn(mutation.addedNodes[i]);
                    }
                }
            }
            else if (mutation.removedNodes.length)
            {
                for (let i = 0; i < mutation.removedNodes.length; i++)
                {
                    if (mutation.removedNodes[i].tagName == 'DIV')
                    {
                        console.log(mutation);
                    }
                }
            }
            else
            {
                console.error(mutation);
            }
        })
    });

    let config = {/*attributes: true,*/ childList: true, /*characterData: true,*/};

    if (chat)
    {
        observer.observe(chat, config);
    }
    else
    {
        console.error('Чат не обнаружен');
    }
}

insertScriptList();
startObserving();
insertControversialStreamInfo();

// Показывает, находится ли стрим под галкой
d.querySelector('.mw-execute-scripts').addEventListener('click', function (e)
{
    e.preventDefault();

    insertControversialStreamInfo();
});

// Получает ссылки на клипы на странице
d.querySelector('.mw-take-clip-url').addEventListener('click', function (e)
{
    e.preventDefault();

    takeLinkToClip();
});

// Сворачивает сообщение
d.querySelector('body').addEventListener('click', function (e)
{
    let myTarget = e.target;
    let minus = myTarget.closest('.mw-minimize-maximize-btn');


    if (minus)
    {
        let messageBlock = minus.closest('.message-block');
        let messageText = messageBlock.querySelector('.message');

        if (!messageBlock.classList.contains('mw-message-minimized'))
        {
            messageBlock.classList.add('mw-message-minimized');

            messageText.setAttribute('data-value', messageText.innerHTML);
            messageText.innerHTML = '<Сообщение скрыто>';

            minus.innerHTML = '+';
        }
        else
        {
            messageBlock.classList.remove('mw-message-minimized');
            messageText.innerHTML = messageText.getAttribute('data-value');

            minus.innerHTML = '‒';
        }
    }
});

// Разворачивает сообщение при клике на свёрнутое сообщение
d.querySelector('body').addEventListener('click', function (e)
{
    let myTarget = e.target;

    let minimizedMessage = myTarget.closest('.message');

    if (minimizedMessage)
    {
        let messageBlock = minimizedMessage.closest('.mw-message-minimized');

        if (messageBlock)
        {
            messageBlock.classList.remove('mw-message-minimized');
            minimizedMessage.innerHTML = minimizedMessage.getAttribute('data-value');
            messageBlock.querySelector('.mw-minimize-maximize-btn').innerHTML = '‒';
        }
    }
});